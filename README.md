# Prototype Voting App

Prototype research project to understand how blockchain applications work using the Ethereum network.

The objective of the project was to research if a blockchain application would be ideal for voting since the votes are in a public ledger that anybody can verify, making the process transparent. The inspiration came from the 2020 elections.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Dependencies

- [NodeJS](https://nodejs.org/en/)
- [Metamask on your browser](https://metamask.io/)
- [Ganache](https://www.trufflesuite.com/ganache)
- [Truffle](https://www.trufflesuite.com/truffle)

### Running locally

1. Start the Ganache application. It will have a server run locally
2. Run the command `truffle migrate`
3. Point Metamask to your local Ganache server
4. Run the command `npm install` on the root of the project
5. Run `npm test` to run the application

You should be able to vote after doing these steps

## Research

### Discoveries

- Deploying code to the Ethereum network is a process that can not be taken lightly. If bugs are introduced, the process of rolling back is difficult.
- Deployed code is public.
- Each vote would need to pay for it to be saved in the network. This could be a way of verifying that only registered voters are able to vote on specific addresses. The information of the voter is hidden to the public, but if someone knows what address belongs to whom, then it won't be confidential anymore.
- Using an external database you can enforce some rules on the voters, but it would need to be decentralized for it to fulfill the goal of transparency and easily verifiable information.
- Vote counts could be streamed live.
- Anybody can run a script to verify the votes, making sure they are correct.

### Conclusion

The technology has a lot of potential. For voting it might not be the best application for now, but for something like an e-commerce site it would be brilliant. The back-end is running on the Ethereum network, saving a lot of money in maintenance for a small company. You still need servers and databases, but less of them.

## Author

* **Ori Alvarez** - *Initial work* - [TheGameFreak720](https://github.com/TheGameFreak720)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

The tutorial used for this prototype was: [The Ultimate Ethereum Dapp Tutorial (How to Build a Full Stack Decentralized Application Step-By-Step)](https://www.dappuniversity.com/articles/the-ultimate-ethereum-dapp-tutorial).
